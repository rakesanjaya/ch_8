import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
  CreateRoom,
  Dashboard,
  GameHistory,
  Login,
  Register,
  VersusCom,
  VersusPlayer,
} from "../../pages";
import Quiz from "../../pages/Quiz";

const Routing = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/createroom" element={<CreateRoom />}></Route>
        <Route path="/dashboard" element={<Dashboard />}></Route>
        <Route path="/gamehistory" element={<GameHistory />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route path="/versuscom" element={<VersusCom />}></Route>
        <Route path="/versusplayer" element={<VersusPlayer />}></Route>
        <Route path="/quiz" element={<Quiz />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Routing;
