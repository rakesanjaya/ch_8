import React from "react";
import "./button.scss";

const Button = ({ buttonName, ...rest }) => {
  return (
    // <div>
    //   <button className="button" {...rest}>
    //     {buttonName}
    //   </button>
    // </div>
    <button type="button" class="btn btn-secondary" {...rest}>
      {buttonName}
    </button>
  );
};

export default Button;
