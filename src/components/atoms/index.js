import Input from "./input";
import Button from "./button";
import Gap from "./spasi";
import Link from "./link";
import InputA from "./inputA";
import InputB from "./inputB";

export { Input, Button, Gap, Link, InputA, InputB };
