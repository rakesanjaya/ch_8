import React from "react";
import "./inputB.scss";

const InputB = ({ label, ...rest }) => {
  return (
    <div className="inputWrapper">
      <p className="labelB">{label}</p>
      <input className="inputB" {...rest} />
    </div>
  );
};

export default InputB;
