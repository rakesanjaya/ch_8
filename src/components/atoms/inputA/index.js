import React from "react";
import "./inputA.scss";

const InputA = ({ label, ...rest }) => {
  return (
    <div className="inputWrapper">
      <p className="label">{label}</p>
      <input className="inputA" {...rest} />
    </div>
  );
};

export default InputA;
