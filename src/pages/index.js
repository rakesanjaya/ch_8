import CreateRoom from "./CreateRoom";
import Dashboard from "./Dashboard";
import GameHistory from "./GameHistory";
import Login from "./Login";
import Register from "./Register";
import VersusCom from "./VersusCom";
import VersusPlayer from "./VersusPlayer";

export {
  CreateRoom,
  Dashboard,
  GameHistory,
  Login,
  Register,
  VersusCom,
  VersusPlayer,
};
