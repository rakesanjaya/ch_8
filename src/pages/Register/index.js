import React from "react";
import "./register.scss";
import { RegisterBg } from "../../assets";
import { Button, Gap, Input, Link } from "../../components";
import { useNavigate } from "react-router-dom";

// import { Button } from "../../components";

const Register = () => {
  const navigate = useNavigate();
  return (
    <div className="mainPage">
      <div className="left">
        <img src={RegisterBg} alt="" className="bgImage" />
      </div>
      <div className="right">
        <p className="title">Register</p>
        <Input label="Username" placeholder="Username" />
        <Gap height={16} />
        <Input label="Email" placeholder="Email" />
        <Gap height={16} />
        <Input label="Password" placeholder="Password" />
        <Gap height={20} />
        <Button buttonName="Register" onClick={() => navigate("/login")} />
        <Gap height={30} />
        <Link title="Return To Login page" onClick={() => navigate("/login")} />
      </div>
    </div>
  );
};

export default Register;
