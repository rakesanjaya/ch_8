import React, { useState } from "react";

export default function Quiz() {
  const [posts, setPosts] = useState([
    {
      title: "Game One",
      player1: "Naryo",
      player1_choice: "Rock",
      player2: "Enggi",
      player2_choice: "Paper",
    },
    {
      title: "Game Two",
      player1: "Edwin",
      player1_choice: "Paper",
      player2: "Enggi",
      player2_choice: "Paper",
    },
    {
      title: "Game Three",
      player1: "Burhan",
      player1_choice: "Scissor",
      player2: "Naryo",
      player2_choice: "Rock",
    },
    {
      title: "Game Four",
      player1: "Ediet",
      player1_choice: "Rock",
      player2: "John",
      player2_choice: "Rock",
    },
    {
      title: "Game Five",
      player1: "Homie",
      player1_choice: "Paper",
      player2: "Enggi",
      player2_choice: "Scissor",
    },
  ]);
  return (
    <div style={{ display: "flex", padding: "20px", flexWrap: "wrap" }}>
      {posts.map((post) => {
        return (
          <div
            style={{
              padding: "10px",
              border: "1px solid lightgreen",
              width: "150px",
            }}
          >
            <div>Player 1 : {post.player1}</div>
            <div>Player 1 choice: {post.player1_choice}</div>
            <div>Player 2 : {post.player2}</div>
            <div>Player 2 choice : {post.player2_choice}</div>
          </div>
        );
      })}
    </div>
  );
}
