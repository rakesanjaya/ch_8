import React from "react";
import "./versuscom.scss";
import { Gap } from "../../components";
import { IconPaper, IconRock, IconScissor, IconVs } from "../../assets";

const VersusCom = () => {
  return (
    <div className="pageVsCom">
      <div className="battleArena">
        BATTLE ARENA
        <Gap height={60} />
      </div>
      <div className="arena">
        <div className="kiri">
          <div className="player1">PLAYER 1</div>
          <Gap height={50} />
          <div className="rock1">
            <img src={IconRock} alt="" />
          </div>
          <Gap height={50} />
          <div className="paper1">
            <img src={IconPaper} alt="" />
          </div>
          <Gap height={50} />
          <div className="scissor1">
            <img src={IconScissor} alt="" />
          </div>
        </div>

        <div className="tengah">
          <div className="tengah1"></div>
          {/* <Gap height={50} /> */}
          <div className="tengah2"></div>
          {/* <Gap height={50} /> */}
          <div className="tengah1">
            <img src={IconVs} alt="" />
          </div>
          {/* <Gap height={50} /> */}
          <div className="tengah1"></div>
        </div>

        <div className="kanan">
          <div className="kanan1">COM</div>
          <Gap height={50} />
          <div className="kanan2">
            <img src={IconRock} alt="" />
          </div>
          <Gap height={50} />
          <div className="kanan3">
            <img src={IconPaper} alt="" />
          </div>
          <Gap height={50} />
          <div className="kanan4">
            <img src={IconScissor} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default VersusCom;
