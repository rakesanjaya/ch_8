import React, { useState } from "react";
import "./dashboard.scss";
import { IconUserName } from "../../assets";
import { Gap, InputA } from "../../components";

export default function Dashboard() {
  const [posts, setPosts] = useState([
    {
      roomName: "Alpha",
      winner: "arya",
    },
    {
      roomName: "Bravo",
      winner: "dustin",
    },
    {
      roomName: "Charlie",
      winner: "jackson",
    },
    {
      roomName: "Delta",
      winner: "st.saturn",
    },
    {
      roomName: "Echo",
      winner: "Valkrie",
    },
  ]);
  return (
    <div className="mainPageDash">
      <div className="left1">
        <div
          style={{
            // border: "2px solid grey",
            width: "500px",
            paddingTop: "10px",
            alignContent: "center",
            borderRadius: "10px",
            backgroundColor: "none",
            marginLeft: "30px",
            marginTop: "20px",
          }}
        >
          <p
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
              fontSize: "30px",
              fontFamily: "'Press Start 2P', cursive",
              color: "white",
            }}
          >
            Player VS Com
          </p>
        </div>
        <div>
          <hr
            style={{
              color: "yellow",
              border: "15px solid",
            }}
          ></hr>
        </div>

        <div
          style={{
            display: "flex",
            padding: "10px",
            flexWrap: "wrap",
          }}
        >
          {posts.map((post) => {
            return (
              <div
                style={{
                  // display: "flex",
                  padding: "5px",
                  border: "5px solid grey",
                  width: "200px",
                  marginLeft: "50px",
                  marginBottom: "50px",
                  borderRadius: "10px",
                }}
              >
                <div
                  style={{
                    fontFamily: "Merriweather",
                    fontWeight: "bold",
                    color: "white",
                  }}
                >
                  Room Name : {post.roomName}
                </div>
                <div
                  style={{
                    fontFamily: "Merriweather",
                  }}
                >
                  Winner: {post.winner}
                </div>
              </div>
            );
          })}
        </div>
        <div className="btnGame">START THE GAME </div>

        <div
          className="btnGame1"
          // style={{
          //   fontSize: "30px",
          //   fontFamily: "'Press Start 2P', cursive",
          //   color: "white",
          //   display: "flex",
          //   justifyContent: "center",
          //   alignItems: "center",
          //   flexDirection: "column",
          //   border: "2px solid grey",
          //   width: "500px",
          //   height: "50px",
          //   marginLeft: "400px",
          //   marginTop: "10px",
          //   backgroundColor: "lightgray",
          //   borderRadius: "10px",
          // }}
        >
          HISTORY
        </div>
      </div>

      <div className="right1">
        <p className="profil">PROFIL</p>
        <div>
          <img className="iconProfil" src={IconUserName} alt="" />
        </div>
        <Gap height={20} />
        <InputA label="Fullname" placeholder="Fullname" />
        <Gap height={20} />
        <InputA label="Phone Number" placeholder="Phone Number" />
        <Gap height={20} />
        <InputA label="Address" placeholder="Address" />
        <Gap height={20} />
        <InputA label="User_id" placeholder="User_id" />
      </div>
    </div>
  );
}
