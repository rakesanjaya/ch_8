import React from "react";
import "./createRoomPage.scss";
import { Gap, InputB, Link } from "../../components";
import { IconPaper, IconRock, IconScissor } from "../../assets";

const Createroom = () => {
  return (
    <div className="background">
      <Gap height={90} />
      <p className="judulpage">CREATE GAME ROOM</p>
      {/* <Gap height={50} /> */}
      <InputB label="" placeholder="input here" />
      <Gap height={90} />
      <div className="pilihan">
        <p className="select">
          SELECT {">"}
          {">"}
        </p>
        <div className="rock">
          <img src={IconRock} alt="" onClick={IconRock} />
        </div>
        <div className="paper">
          <img src={IconPaper} alt="" onClick={IconPaper} />
        </div>
        <div className="scissor">
          <img src={IconScissor} alt="" onClick={IconScissor} />
        </div>
      </div>
      <Gap height={90} />
      <div className="btnGame3">SAVE</div>
    </div>
  );
};

export default Createroom;
