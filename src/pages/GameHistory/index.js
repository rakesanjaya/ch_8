import React, { useState } from "react";
import "./gameHistory.scss";
import { Gap } from "../../components";

const GameHistory = () => {
  const [post] = useState([
    {
      roomName: "alpha",
      date: "june 13th 01.00",
      status: "win",
    },

    {
      roomName: "bravo",
      date: "june 14th 01.00",
      status: "lose",
    },
    {
      roomName: "charlie",
      date: "june 15th 01.00",
      status: "draw",
    },
    {
      roomName: "delta",
      date: "june 15th 01.00",
      status: "draw",
    },
    {
      roomName: "echo",
      date: "june 15th 01.00",
      status: "draw",
    },
    {
      roomName: "foxtrot",
      date: "june 15th 01.00",
      status: "draw",
    },
  ]);
  return (
    <div className="pageHistory">
      <div className="bgHistoryPage">
        <Gap height={50} />
        <div className="judulPageHistory">GAME HISTORY</div>
        <Gap height={50} />
        <div
          style={{
            display: "flex",
            border: "1px solid black",
            width: "700px",
            justifyContent: "center",
            alignContent: "center",
            marginLeft: "550px",
          }}
        >
          <div className="box1">
            {post.map((post) => {
              return (
                <div className="box1a">
                  <div className="box1aFont">{post.roomName}</div>
                </div>
              );
            })}
          </div>

          <div className="box2">
            {post.map((post) => {
              return (
                <div className="box2a">
                  <div className="box2aFont">{post.date}</div>
                </div>
              );
            })}
          </div>
          <div className="box3">
            {post.map((post) => {
              return (
                <div className="box3a">
                  <div className="box3aFont">{post.status}</div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default GameHistory;
