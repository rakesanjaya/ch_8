import React from "react";
import { LoginBg } from "../../assets";
import { Button, Gap, Input, Link } from "../../components";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  return (
    <div className="mainPage">
      <div className="left">
        <img src={LoginBg} alt="" className="bgImage" />
      </div>
      <div className="right">
        <p className="title">LOGIN</p>
        <Input label="Username" placeholder="Username" />
        <Gap height={16} />
        <Input label="Email" placeholder="Email" />
        <Gap height={16} />
        <Input label="Password" placeholder="Password" />
        <Gap height={20} />
        <Button buttonName="LOG IN" onClick={() => navigate("/register")} />
        <Gap height={30} />
        <Link
          title="Register for new member"
          onClick={() => navigate("/register")}
        />
      </div>
    </div>
  );
};

export default Login;
