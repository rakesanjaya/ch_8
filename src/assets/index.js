import RegisterBg from "./image/registerBg.jpg";
import LoginBg from "./image/loginBg.jpg";
import IconUserName from "../assets/icon/username-icon.png";
import IconRock from "../assets/icon/rock.png";
import IconPaper from "../assets/icon/paper.png";
import IconScissor from "../assets/icon/scissor.png";
import IconVs from "../assets/icon/vs.png";

export {
  RegisterBg,
  LoginBg,
  IconUserName,
  IconRock,
  IconPaper,
  IconScissor,
  IconVs,
};
